# Journal Network Viewer - Hosted Visualizations

This page can be referenced by the DOI https://doi.org/10.17617/1.FBYY-GV80

This page hosts network visualizations created with the 
[Journal Network Viewer](https://gitlab.mpcdf.mpg.de/cboul/journal-network-viewer/) script.

## Journal of Law and Society

The following two network visualizations show the difference in data coverage between the bibliometric databases OpenAlex and Web of Science (WoS). Both show the citation network of the Journal of Law and Society (JLS), built from the 100 most cited journals in JLS.

- [Journal of Law and Society (OpenAlex)](journal-network-jls-openalex.html)
- [Journal of Law and Society (Web of Science)](journal-network-jls-wos.html)

_The WoS and OpenAlex data were provided by the German Competence Network for Bibliometrics, funded by the German Federal Ministry of Education and Research (Grant 16WIK2101A)._

The visualizations are referenced and explained in Boulanger, C., Creutzfeldt, N. and Hendry, J. (2024) The Journal of Law and Society in Context: Network Analysis of Citations, Journal of Law and Society Blog. Available at: [https://journaloflawandsociety.co.uk/blog/the-journal-of-law-and-society-in-context-network-analysis-of-citations](https://journaloflawandsociety.co.uk/blog/the-journal-of-law-and-society-in-context-network-analysis-of-citations), archived at [https://zenodo.org/doi/10.5281/zenodo.10807615](https://zenodo.org/doi/10.5281/zenodo.10807615)

- Fig. 6: [Cluster "Criminology, Law, Social Science" (Source: Web of Science)](journal-network-jls-wos.html#minEdgeValue%3D67%26selectedGroups%3D4) 
- Fig. 7: [Cluster "Law and Society, Legal Studies, Social Sciences" (Source: OpenAlex.org)](journal-network-jls-openalex.html#minEdgeValue%3D67%26selectedGroups%3D5)

