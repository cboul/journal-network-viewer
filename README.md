# Journal Network Viewer 

Visualization DOI: https://doi.org/10.17617/1.FBYY-GV80

This repo contains the R code for generating an interactive viewer for a journal citation network together with a 
UI to filter the displayed nodes and edges. 

See the [network visualizations hosted on this page](https://cboul.pages.mpcdf.de/journal-network-viewer/) for examples. 



All scripts in the repository have been written with heavy support by ChatGPT4, which has taught me an incredible amount
of doing data science with R along the way. All errors are mine. The development has also been assisted by the 
DataSpell IDE from JetBrains using an Open Source License. 

## Using the search/filter popup

The generated network viewer comes with a user interface to filter the complete network down to the parts that are
most interesting for analysis. You can search for individual journals, exclude relationships (edges) below a
specific relevance (the normalized weight) and display clusters of related journals, which have been
automatically identified from the citation relationships.

## Prerequisites

- R 4.2.2 (a lower version might work)
- Install the R packages listed at the top of [generate-journal-network-viewer.R](generate-journal-network-viewer.R).

# Steps to generate a visualization:

The script expects a CSV file having the following columns:

```CSV
"source_title_citing","source_title_cited","citation_year","count_citations"
```

To generate your visualizations, have a look at [main.R](main.R).  Call the `generate_journal_network_viewer()`
function with the appropriate values. 


## Description of the algorithm

The script transforms the given CSV data into a list of node and edges, enriching them with statistical data that is
displayed when hovering over the nodes and edges. In order to remove insignificant edges, a threshold value can be
specified that removes citation relationships below a minimum number of citations over the whole period. 

### Normalized citations weights

In order to assess the significance of the citation relationships,  the ratio of citations from one journal to 
another per year is computed, normalized by the total number of citations in the citing journal. This normalized weight 
is used to  determine the thickness of the edges, and also plays a role in clustering the nodes as described further 
below. 

The distribution of this data is heavily right-skewed, i.e. most of the observations occur near zero:

![jls-openalex-weight_distribution_plot.png](public%2Fjls-openalex-weight_distribution_plot.png){height=200px}

The JNV uses a slider to filter edges that fall below a certain significance. This type of distribution is not well
suited to a slider, which is normally used to navigate within a linear range of values. In order to have linear values
for the slider, and to further analyze the nature of the distribution, I applied a logarithmic transformation and 
spread the values on a scale of 0-99:

```math
w' = \log(w^-1 + 1)
```
```math
w'' = n - \text{round}( \frac{w' - \min(w')}{\max(w') - \min(w')} \cdot n )
```


This results in the following distribution:

![jls-openalex-scaled_adjusted_log_transformed_weight_distribution_plot.png](public%2Fjls-openalex-scaled_adjusted_log_transformed_weight_distribution_plot.png){height=200px}

which suggests that the distribution is a [Pareto distribution](https://en.wikipedia.org/wiki/Pareto_distribution). As there is no linear decrease in the log-transformed
distribution, it does not follow ["Zipf's Law"](https://en.wikipedia.org/wiki/Zipf%27s_law).

### Clustering / community detection

We want to automatically cluster the journal nodes according to their "closeness" (as per the citations in the articles 
published). A common algorith to achieve this is the [Louvain community detection method](https://en.wikipedia.org/wiki/Louvain_method).
The algorithm used requires that our directed graph needs to be [transformed into an undirected one](generate-journal-network-viewer.R#L212) 
in which the edges have no direction, which also aids readability of the graph. In this process, the normalized weights 
are summed up and the directional information is stored as additional properties. 

This graph is then [clustered using `cluster_louvain()`](generate-journal-network-viewer.R#L259), which stores an integer
value in the `group` property. The number of groups that are created cannot be directly set, but can be influenced
by the `resolution` parameter of the `cluster_louvain()` method.

To generate a human-readable title for the resulting groups automatically, the journal titles in a group [are sent to 
the OpenAi.org API](query-open-ai-api.R#L67).
