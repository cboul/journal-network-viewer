-- sql queries

drop table if exists DB_USER.DB_TABLE_PREFIX_items;
create table if not exists DB_USER.DB_TABLE_PREFIX_items as
select * from DB_SCHEMA.items i
where i.source_title = :source_title;

drop table if exists DB_USER.DB_TABLE_PREFIX_citing;
create table if not exists DB_USER.DB_TABLE_PREFIX_citing as
select *
from DB_USER.DB_TABLE_PREFIX_items i
         inner join DB_SCHEMA.refs r
                    on i.item_id = r.item_id_citing;

drop table if exists DB_USER.DB_TABLE_PREFIX_cited;
create table if not exists DB_USER.DB_TABLE_PREFIX_cited as
select *
from DB_SCHEMA.items i
         inner join DB_SCHEMA.refs r on i.item_id = r.item_id_citing
where r.ref_source_title = :source_title;

drop table if exists DB_USER.DB_TABLE_PREFIX_journals_cited;
create table if not exists DB_USER.DB_TABLE_PREFIX_journals_cited as
select source_title, count from (
                                    SELECT lower(source_title) as source_title, COUNT(*) AS count
                                    FROM DB_USER.DB_TABLE_PREFIX_cited
                                    WHERE source_title !~ '[0-9.]'
                                    GROUP BY lower(source_title)
                                    order by count desc
                                ) as s
where s.count >= 10;

--
-- Create Temporary Table for Cited Journals
CREATE TEMP TABLE temp_cited_journals AS
SELECT DISTINCT lower(source_title) AS source_title
FROM DB_USER.DB_TABLE_PREFIX_journals_cited;

-- Get all articles from the journals in this list
drop table if exists DB_USER.DB_TABLE_PREFIX_items_in_network;
CREATE TABLE IF NOT EXISTS DB_USER.DB_TABLE_PREFIX_items_in_network AS
SELECT *
FROM DB_SCHEMA.items i
WHERE lower(i.source_title) IN (SELECT source_title FROM temp_cited_journals);


-- Get a table of journals which cite each other, with number of citations per year
drop table if exists DB_USER.DB_TABLE_PREFIX_sociolegal_journal_network;
CREATE TABLE IF NOT EXISTS DB_USER.DB_TABLE_PREFIX_sociolegal_journal_network AS
SELECT
    lower(i1.source_title) AS source_title_citing,
    lower(i2.source_title) AS source_title_cited,
    i1.pubyear AS citation_year,
    COUNT(*) AS count_citations
FROM DB_USER.DB_TABLE_PREFIX_items_in_network i1
         JOIN DB_SCHEMA.refs r ON r.item_id_citing = i1.item_id
         JOIN DB_USER.DB_TABLE_PREFIX_items_in_network i2 ON r.item_id_cited = i2.item_id
--where i1.source_type = 'Journal' and i2.source_type = 'Journal'
GROUP BY
    i1.source_title,
    i2.source_title,
    i1.pubyear;
